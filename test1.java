package sele;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;  
import org.openqa.selenium.Dimension;	
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.JavascriptExecutor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

class Test1 {

	public static void main(String[] args) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, FileNotFoundException, IOException  {
		
		File file= new File("C:\\Users\\Admin\\Desktop\\flight.xls");
		FileInputStream fis= new FileInputStream(file);
		Workbook wb= WorkbookFactory.create(fis);
		
		Sheet sh = wb.getSheet("sheet1");
        String dataarray[];
        dataarray = new String[6];
		
		int rowcount = sh.getLastRowNum();
        for (int i = 0; i <= rowcount; i++) 
        {
          int cellcount = sh.getRow(i).getLastCellNum();
             for (int j = 0; j < cellcount; j++) 
             {                         
               String value = sh.getRow(i).getCell(j).getStringCellValue();
	           dataarray[j]= sh.getRow(i).getCell(j).getStringCellValue();
	           //System.out.println(dataarray[j]);
             }			
          }		
		
		String depart=sh.getRow(1).getCell(0).getStringCellValue();
		String arrive= sh.getRow(1).getCell(1).getStringCellValue();
		String datetravel=sh.getRow(1).getCell(2).getStringCellValue();
		
		System.out.println(depart);
		System.out.println(arrive);
		System.out.println(datetravel);
				
		//Exe calling
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
				
		//Chrome driver initiation 
		 ChromeDriver  driver = new ChromeDriver();
		  
		//Going to the website  
		 driver.navigate().to("https://www.trujet.com/#/home");  
		 driver.manage().window().maximize();
		 
		//Click on the from box  
		driver.findElement(By.id("Fromval")).sendKeys(depart);
		
		((JavascriptExecutor)driver).executeScript("window.scrollTo(10,10)");
		Thread.sleep(2000);
		
		//Click on the to box 
		driver.findElement(By.id("Toval")).sendKeys(arrive);

		((JavascriptExecutor)driver).executeScript("window.scrollTo(10,10)");
		Thread.sleep(2000);

		//Set the date 
		driver.findElement(By.xpath("//*[@id=\"oneWay\"]/div/form/div/div/div[3]/div/div[1]/input")).sendKeys(datetravel);
	
		((JavascriptExecutor)driver).executeScript("window.scrollTo(10,10)");
		Thread.sleep(2000);
		
		//Search for flights 
		driver.findElement(By.xpath("//a[contains(text(),\"Search\")]")).click();
		Thread.sleep(10000);
		
		//Sort by duration 
		driver.findElement(By.xpath("//span[contains(text(),\"Duration\")]")).click();
		Thread.sleep(5000);
		
		
		//Select the flight
		driver.findElement(By.xpath("//span[contains(text(),\"Select flight\")]")).click();
		Thread.sleep(5000);
		 
		//Quit the driver 
		 driver.quit();
	}

}